# Ransomware

---

## `kaetzchen2.svr`

- 32bit PE executable
- seems packed
- VirusTotal: `Xorist`

---

### Behaviour
- persists in `%TEMP%`
- adds itself to autorun
- encrypts all (matching) files in all drives
- shows ransom message (50 EUR via Ukash/Paysafe)
- asks user for a password

---

### Unpacking
- layer 1: UPX, sections renamed
    - we did miss this
- layer 2: RunPE (Visual Basic 6)
    - unpacked with `x32dbg` + `Scylla`
- layer 3: UPX, modified
    - unpacked with `deUPX`

---

### Patching
- patch the malware to decrypt files
- multiple possibilities

---

![before the patch](patch_pre.png)

---

![check #1](patch_target_1.png)
![check #2](patch_target_2.png)

---

![after the patch](patch_post.png)

---

### Patched usage
- not too my wrong passwords before
- now spam the "OK" button
- instead of scrambling it will decrypt the files

---

### Malware intel

---

#### Config
- loaded from resource (`.rsrc` section)
- first 16 bytes are XOR key
- decrypts rest of it with that key

---

![config](config.png)

---

```
*.zip, *.rar, *.7z, *.tar, *.gzip, *.jpg, *.jpeg, *.psd, 
*.cdr, *.dwg, *.max, *.bmp, *.gif, *.png, *.doc, *.docx,
*.xls, *.xlsx, *.ppt, *.pptx, *.txt, *.pdf, *.djvu, *.htm,
*.html, *.mdb, *.cer, *.p12, *.pfx, *.kwm, *.pwm, *.1cd,
*.md, *.mdf, *.dbf, *.odt, *.vob, *.ifo, *.lnk, 
*.torrent, *.mov, *.m2v, *.3gp, *.mpeg, *.mpg, *.flv, 
*.avi, *.mp4, *.wmv, *.divx, *.mkv, *.mp3, *.wav, 
*.flac, *.ape, *.wma, *.ac3
```

---


> Attention! All your files are encrypted!
> You are using unlicensed programms!
> To restore your files and access them,
> send code Ukash or Paysafecard nominal value of  EUR 50 to the e-mail Koeserg@gmail.com. During the day you receive the answer with the code.
> You have 5 attempts to enter the code. If you exceed this
> of all data irretrievably spoiled. Be
> careful when you enter the code!


---

![builder](builder_tool_pcrisk.jpg)

---

#### User-supplied password

![prompt](password_prompt.png)

- `md5(md5(md5(md5(md5(password)))))`
- compared to hash from config
- mitigated by removing the check

---

#### Too many failed attempts
- files get scrambled
- after 5 wrong passwords
- generates random key
- *decrypts* files with that
- FAIL: did not work at all
- FAIL: key is weak
- recover files by bruteforcing key

---

#### Crypto functions
- `tea_encrypt`
    - TEA encrypt implementation
- `tea_decrypt`
    - TEA decrypt implementation
- `xor_crypt`
    - used as an alternative to crypt files
- `xor_decrypt`
    - decrypts the config
- `hash_md5`
    - uses APIs to do MD5

---

#### Crypto keys
- `xor_key`
    - for config and file encryption (XOR)
- `file_master_key`
    - used for key derivation
    - derived by swapping endianness
- `file_key`
    - used for actual file crypting
    - derived from master key and filename
- scrambling keys
    - 2³² possible
    - plus derived file keys

---

#### Well known crypto
- Tiny Encryption Algorithm
- block cipher
- Feistel structure
- 32 cycles
- 128bit key
- 64bit block size
- found by searching the constant (`0x9E3779B9`)
- has several flaws and successors
    - equivalent keys
    - XTEA, XXTEA
- is used in ECB mode
---

![tea](tea_feistel.png)

---

### Emulation
- emulate decryption routine
- we used Unicorn Engine

---

```py
#!/usr/bin/env python

import sys
import struct
from ntpath import basename
from unicorn import *
from unicorn.x86_const import *
from idaapi import *
from idautils import *
from idc import *


class Emulator():
    def __init__(self, code):
        self.code = code
        self.code_addr = 0x401000
        self.key_addr = 0x40658d
        self.init_emulator()

    def init_emulator(self):
        # init unicorn engine
        self.mu = Uc(UC_ARCH_X86, UC_MODE_32)
        page_size = 4 * 1024

        # map + write code
        self.mu.mem_map(self.code_addr, page_size)
        self.mu.mem_write(self.code_addr, self.code)

        # map data
        self.mu.mem_map(0x406000, page_size)
        # write the round count constant 
        self.mu.mem_write(self.key_addr+24, struct.pack('<I', 32))

    def emulate(self, dword1, dword2, key):
        # write the key
        self.mu.mem_write(self.key_addr, key)

        # set registers
        self.mu.reg_write(UC_X86_REG_EAX, dword1)
        self.mu.reg_write(UC_X86_REG_EDX, dword2)

        # run the emulation
        self.mu.emu_start(self.code_addr, self.code_addr + len(self.code))

        # retrieve values
        r_eax = self.mu.reg_read(UC_X86_REG_EAX)
        r_edx = self.mu.reg_read(UC_X86_REG_EDX)

        return r_eax, r_edx

    def decrypt_block(self, block, key):
        dword1, dword2 = struct.unpack('<II', block)
        hi, lo = self.emulate(dword1, dword2, key)
        return struct.pack('<II', hi, lo)


def rol(val, n, bits=32):
    upper = val & (((1 << n)-1) << (bits-n))
    lower = val & ((1 << (bits-n))-1)
    return (lower << n) + (upper >> (bits-n))


def derive_file_key(file_master_key, filename):
    # take the first char of the filename
    tmp = ord(filename[0])

    file_key = list(file_master_key)
    for i in range(len(file_key)):
        file_key[i] = ord(file_key[i]) ^ tmp
        tmp = rol(tmp, 1, bits=8)

    return bytes(file_key)


def get_function(start, end):
    code = b''
    for head in Heads(start, end+1):
        size = ItemSize(head)
        code += GetManyBytes(head, size)
    return code


def decrypt_data(key, data):
    code = get_function(0x4018be, 0x40195e)
    emu = Emulator(code)

    plain = b''
    for i in range(0, len(data), 8):
        data_block = data[i:i+8]
        if len(data_block) == 8:
            # its a full block, decrypt it
            plain_block = emu.decrypt_block(data_block, key)
        else:
            # its the last block and its not full block size, so it was not encrypted
            plain_block = data_block
        plain += plain_block

    return plain


def decrypt_file(file_master_key, file_path, skip_bytes):
    # get the filename
    filename = basename(file_path)
    # derive the file-specific key
    file_key = derive_file_key(file_master_key, filename)

    with open(file_path, 'rb') as f:
        # read the file content
        data = f.read()
        # skip some bytes since they are unencrypted
        untouched = data[:skip_bytes]
        encrypted = data[skip_bytes:]
        # decrypt the rest if there is any
        if len(encrypted) > 0:
            return untouched + decrypt_data(file_key, encrypted)
        else:
            return untouched


def main():
    # CHANGE THESE
    file_path     = 'E:\\important.txt.EnCiPhErEd'
    file_path_out = 'C:\\Users\\syssec\\Desktop\\important.txt'

    # the master key to derive the file keys from
    file_master_key = b'\x46\x94\x2e\x4a'+b'\x5b\x85\x64\x60'+b'\x8c\x89\x86\x5a'+b'\x50\x6c\x63\x7f'
    # the number of unencrypted bytes at the beginning of a file
    skip_bytes = 0x47

    # decrypt the file and output the plaintext
    decrypted = decrypt_file(file_master_key, file_path, skip_bytes)

    # write the decrypted file
    with open(file_path_out, 'wb') as f:
        f.write(decrypted)

    print('Decryption done!')


if __name__ == '__main__':
    main()

```

---

### Other stuff
- uses file times for camouflage
- can change the wallpaper
- registers for file extension
- removes itself after decryption
- uses `Windows-1251` charset (cyrillic)

---

# Thank you!

### Questions?
